# My Website
Across my lifetime, I have tried numerous mechanisms for my website with a basic blog system, including [WordPress](https://wordpress.org), [Pelican](https://blog.getpelican.com), [Jekyll](https://jekyllrb.com/), and even a home-grown solution on [Google App Engine](https://cloud.google.com/appengine/). I have now settled on [MkDocs](https://www.mkdocs.org/), which is definitely not intended for blogging, but I find it well-suited for the task.
## Getting Started
I installed mkdocs with `#!bash pip install mkdocs` and created a new project using `#!bash mkdocs new .`. I also initialised a [Git repository on GitLab](https://gitlab.com/retnikt/retnikt.gitlab.io) to version all the configuration and content.
## Customisation
Now it's time for some configuration. 
### Theme
The first thing to do is pick a theme. I chose the popular and brilliant [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).
![mkdocs-material](https://i.imgur.com/BjcuAVf.png)
To install, you can follow [this guide](https://squidfunk.github.io/mkdocs-material/) for detailed instructions, but essentially: you use pip. (`#!bash pip install mkdocs-material`)

To enable this theme, we need to add it to the mkdocs.yml configuration file:
???+ abstract "Code"
    ```yaml
    theme:
      name: 'material'
      custom_dir: 'theme'
    ```
The `custom_dir` line is so that we can override parts of the HTML.

We can make some tweaks to the rest of the appearance too, including colours, fonts, and [many other options](https://squidfunk.github.io/mkdocs-material/getting-started/#configuration). Here's my final `theme` section:

???+ abstract "Code"
    ```yaml
    theme:
      name: 'material'
      custom_dir: 'theme'
      language: en
      palette:
        primary: 'blue'
        accent: 'blue'
      font:
        text: 'Noto Sans'
        code: 'Fira Code'  # needs some [extra setup](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/main.html#L4) to make this work
      logo:
        icon: 'developer_board'
      feature: false
      favicon: 'assets/images/favicon2.png'
    ```


### Extra Options
First let's set some basic options:

???+ abstract "Code"
    ```yaml
    site_name: retnikt
    site_url: 'https://retnikt.uk/'
    site_dir: 'public'  # this is needed for GitLab pages
    ```

I'm going to add a copyright to the footer and some social media links, and enable a few (lots of) Markdown extensions:

???+ abstract "Code"
    ```yaml
    copyright: '© retnikt 2019'
    extra:
      social:
        - type: 'github'
          link: 'https://github.com/retnikt'
        - type: 'twitter'
          link: 'https://twitter.com/retnikt'
    markdown_extensions:
      - admonition
      - codehilite
      - footnotes
      - meta
      - pymdownx.arithmatex
      - pymdownx.betterem:
          smart_enable: all
      - pymdownx.caret
      - markdown.extensions.attr_list
      - markdown.extensions.def_list
      - markdown.extensions.tables
      - markdown.extensions.abbr
      - pymdownx.extrarawhtml
      - pymdownx.critic
      - pymdownx.details
      - pymdownx.emoji:
          emoji_generator: !!python/name:pymdownx.emoji.to_svg
      - pymdownx.inlinehilite
      - pymdownx.magiclink
      - pymdownx.mark
      - pymdownx.smartsymbols
      - pymdownx.superfences
      - pymdownx.progressbar
      - pymdownx.keys
      - pymdownx.tasklist:
          custom_checkbox: true
      - pymdownx.tilde
      - toc:
          permalink: true
    ```
### Theme Overrides
I want to customise some of the theme components, particularly the footer. here is a list of all my tweaks, the sources for which you can find [on GitLab](https://gitlab.com/retnikt/retnikt.gitlab.io/):

* Reference GitLab Pages in the footer (see [Deploying](#deploying) below) ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/footer.html#L54))
* Hide Next/Previous page arrows on the homepage ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/partials/footer.html#L3))
* Change the font stylesheet links to enable Fira Code ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/main.html#L4))
* Change the top of the navigation on the left to say "Navigation" instead of the page title ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/partials/nav.html#L28))
* Change "Table of contents" to just "Contents" ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/partials/language/en.html#L22))
* Change the 404 page content ([code](https://gitlab.com/retnikt/retnikt.gitlab.io/blob/master/theme/404.html#L3))

## Building
To build the site locally, run `mkdocs build`. You can also use the live preview web server built in to mkdocs with `mkdocs serve`.
### Deploying
I am deploying my site on GitLab Pages, taking advantage of its continuous integration for building the site, similar to how GitHub can automatically deploy Jekyll sites. I based my `.gitlab.ci.yml` file on [this example](https://gitlab.com/pages/mkdocs/blob/master/.gitlab-ci.yml). Note that you need to set the `site_dir` value in `mkdocs.yml` to `public` so that GitLab Pages can deploy, because it looks for the HTML in the CI `public` artifact folder. I set up my custom domain [retnikt.uk](https://retnikt.uk/) ([instructions](https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/)) and finalised my configuration.
