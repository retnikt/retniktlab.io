# Supermicro IPMIView

The Supermicro IPMIView tool for X9, (maybe X8, X10 and X11) motherboards' GUI IPMI Viewer
tool for Linux is extremely difficult to find online. These instructions should work for
any Linux distribution, and maybe even MacOS or BSD?

The Supermicro IPMI Viewer tool allows you to view and interact with the network keyboard, video,
and mouse connection (KVM over IP) and remote Serial console, remotely shutdown, reboot, and
startup the system, manage users, view sensor readings and event logs, and more.  
Most of these tasks can be accomplished using the Web Interface over HTTP on Port 80, but
the viewer offers many advantages such as encryption, no Java browser plugin required, and
management of groups of saved systems.

### Instructions

Simply download [this](https://www.supermicro.com/wftp/utility/IPMIView/Linux/IPMIView_2.16.0_build.190815_bundleJRE_Linux_x64.tar.gz)
and extract the archive to a folder of your choice, typically something like
`~/.local/share/applications` or `/opt`. You can then run it with the `IPMIView20` script.

??? note "SHA512 sum"
    `9a6af293a08dcdb662a226c061590e7149e5cee0a69eca12906e4581e254d3293d27e71dd36c301a449a43a10be700f3023f8408bdda5a3b9b65d76c96eaa3d7`

### Arch - Note
If you are are using Arch Linux or derivatives, you can get IPMIView from the AUR as `ipmiview`.
The rest of these instructions are strongly based on that package.
